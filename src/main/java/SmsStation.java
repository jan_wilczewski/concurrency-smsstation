import java.util.LinkedList;
import java.util.List;
import java.util.Observer;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

/**
 * Created by jan_w on 28.11.2017.
 */
public class SmsStation {

    private ExecutorService antena = Executors.newFixedThreadPool(10);
    SmsRequest smsRequest = new SmsRequest();

    private List<Telephone> phoneList = new LinkedList<>();

    public void addPhone(Telephone t){
        phoneList.add(t);
    }

    public void addObserversToRequest(){
        for (Telephone tel: phoneList){
            smsRequest.addObserver(tel);
        }
    }

    public void sendSms (String text, String number){
        smsRequest.setSMS(text, number);
        antena.submit(smsRequest);
    }

}
