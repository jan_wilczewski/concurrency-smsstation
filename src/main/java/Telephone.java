import java.util.LinkedList;
import java.util.List;
import java.util.Observable;
import java.util.Observer;

/**
 * Created by jan_w on 28.11.2017.
 */
public class Telephone implements Observer {

    private final String phoneNumber;

    public Telephone(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    private final List<SmsRequest> requestList = new LinkedList<>();

    @Override
    public void update(Observable o, Object arg) {
        if (arg instanceof SmsRequest){
            SmsRequest message = (SmsRequest) arg;
            if (message.getNumber().equals(phoneNumber)){
                requestList.add(message);
                System.out.println("" + phoneNumber + ": SmsMessage: " + message.getText() + " received");
            }
            else {
                System.out.println("" + phoneNumber + " SmsMessage: " + message.getText() + " ignored");
            }
        }
    }
}
