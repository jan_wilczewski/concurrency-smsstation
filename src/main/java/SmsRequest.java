import java.util.Observable;
import java.util.Observer;

/**
 * Created by jan_w on 28.11.2017.
 */
public class SmsRequest extends Observable implements Runnable {

    private String text;
    private String number;

    public void addPhone(Telephone t){
        addObserver(t);
    }


    @Override
    public void run() {

        char[] textSize = text.toCharArray();
        long textSigns = textSize.length;

        try {
            Thread.sleep(100 * textSigns);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        SmsRequest message = new SmsRequest(text, number);
        setChanged();
        notifyObservers(message);

        System.out.println("SMS został wysłany");
    }

    public void setSMS(String text, String number){
        this.text = text;
        this.number = number;
    }

    public SmsRequest(String text, String number) {
        this.text = text;
        this.number = number;
    }

    public SmsRequest() {
    }

    public String getText() {
        return text;
    }

    public String getNumber() {
        return number;
    }
}
