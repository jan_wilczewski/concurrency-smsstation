import com.sun.org.apache.xpath.internal.SourceTree;

import java.util.Scanner;

/**
 * Created by jan_w on 28.11.2017.
 */
public class Main {

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        System.out.println("Wpisz numer telefonu i treść wiadomości: ");
        String[] sms = scanner.nextLine().split(", ");
        String number = sms[0];
        String text = sms[1];

        SmsStation smsStation = new SmsStation();

        smsStation.addPhone(new Telephone("111111"));
        smsStation.addPhone(new Telephone("222222"));
        smsStation.addPhone(new Telephone("333333"));

        smsStation.addObserversToRequest();
        smsStation.sendSms(text, number);


    }


}
